package com.example.fos;

import com.example.fos.db.example.ExampleDrinkProvider;
import com.example.fos.db.example.ExampleMealProvider;
import com.example.fos.db.example.ExampleProductProvider;
import com.example.fos.io.*;
import org.apache.commons.lang3.StringUtils;

import java.io.PrintStream;

public class FoodOrderingSystem {
    public static void main(String[] args) {
        String command = StringUtils.join(args, " ");
        buildCommandChain().process(command);
    }

    private static Command buildCommandChain() {
        PrintStream printStream = System.out;
        Command help = new HelpCommand(printStream);
        Command lunch = new LunchCommand(printStream, new ExampleMealProvider());
        Command drink = new DrinkCommand(printStream, new ExampleDrinkProvider());
        Command get = new GetCommand(printStream, new ExampleProductProvider());

        help.setSuccessor(lunch);
        lunch.setSuccessor(drink);
        drink.setSuccessor(get);

        return help;
    }
}
