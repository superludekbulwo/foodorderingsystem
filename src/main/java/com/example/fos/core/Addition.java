package com.example.fos.core;

public class Addition extends Product {
    public Addition(String name, double price) {
        super(name, price);
    }

    @Override
    public ProductType getType() {
        return ProductType.ADDITION;
    }
}
