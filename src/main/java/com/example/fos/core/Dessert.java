package com.example.fos.core;

public class Dessert extends Meal {
    public Dessert(String name, double price, String cuisine) {
        super(name, price, cuisine);
    }

    @Override
    public ProductType getType() {
        return ProductType.DESSERT;
    }
}
