package com.example.fos.core;

public class Drink extends Product {
    public Drink(String name, double price) {
        super(name, price);
    }

    @Override
    public ProductType getType() {
        return ProductType.DRINK;
    }
}
