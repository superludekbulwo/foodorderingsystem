package com.example.fos.core;

public class MainCourse extends Meal {
    public MainCourse(String name, double price, String cuisine) {
        super(name, price, cuisine);
    }

    @Override
    public ProductType getType() {
        return ProductType.MAIN_COURSE;
    }
}
