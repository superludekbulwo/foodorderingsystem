package com.example.fos.core;

public abstract class Meal extends Product {

    protected final String cuisine;

    public Meal(String name, double price, String cuisine) {
        super(name, price);
        if (cuisine == null || cuisine.isEmpty())
            throw new IllegalArgumentException("name cannot be null or empty");
        this.cuisine = cuisine;
    }

    public String getCuisine() {
        return cuisine;
    }
}