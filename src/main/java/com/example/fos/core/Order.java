package com.example.fos.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class Order {
    private final Collection<Product> products;

    private Order(Collection<Product> products) {
        this.products = Collections.unmodifiableCollection(products);
    }

    public Collection<Product> getProducts() {
        return products;
    }

    public static class Builder {
        private final Collection<Product> products;

        public Builder() {
            this.products = new ArrayList<>();
        }

        public Builder addProduct(Product product) {
            if (product == null)
                throw new IllegalArgumentException("product cannot be null");

            this.products.add(product);

            return this;
        }

        public Order build() {
            return new Order(products);
        }
    }
}
