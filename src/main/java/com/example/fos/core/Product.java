package com.example.fos.core;

public abstract class Product {
    protected String name;
    protected double price;


    public Product(String name, double price) {
        if (name == null || name.isEmpty())
            throw new IllegalArgumentException("name cannot be null or empty");
        this.name = name;
        if (price < 0.0)
            throw new IllegalArgumentException("price cannot be less than 0");
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public abstract ProductType getType();
}
