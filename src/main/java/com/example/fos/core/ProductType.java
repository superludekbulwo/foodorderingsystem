package com.example.fos.core;

public enum ProductType {
    MAIN_COURSE,
    DESSERT,
    DRINK,
    ADDITION
}
