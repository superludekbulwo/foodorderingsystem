package com.example.fos.db;

import com.example.fos.core.Addition;
import com.example.fos.core.Drink;

import java.util.Collection;

public interface DrinkProvider {
    Collection<Drink> getDrinks();

    Collection<Addition> getAdditions();
}
