package com.example.fos.db;

import com.example.fos.core.MainCourse;
import com.example.fos.core.Dessert;

import java.util.Collection;

public interface MealProvider {
    Collection<MainCourse> getMainCourses();
    Collection<Dessert> getDessert();
}
