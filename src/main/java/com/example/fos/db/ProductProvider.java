package com.example.fos.db;

import com.example.fos.core.Product;

import java.util.Collection;

public interface ProductProvider {
    Collection<Product> getProducts();
}
