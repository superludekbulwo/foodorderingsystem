package com.example.fos.db.example;

import com.example.fos.core.Addition;
import com.example.fos.core.Drink;
import com.example.fos.db.DrinkProvider;

import java.util.ArrayList;
import java.util.Collection;

public class ExampleDrinkProvider implements DrinkProvider {

    private final static Collection<Drink> drinks = new ArrayList<Drink>() {{
        add(new Drink("Pepsi", 2.5));
        add(new Drink("Fanta", 1.9));
    }};

    private final static Collection<Addition> additions = new ArrayList<Addition>() {{
        add(new Addition("Ice cubes", 0.2));
        add(new Addition("Lemon", 0.5));
    }};

    @Override
    public Collection<Drink> getDrinks() {
        return drinks;
    }

    @Override
    public Collection<Addition> getAdditions() {
        return additions;
    }
}
