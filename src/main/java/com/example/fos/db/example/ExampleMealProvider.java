package com.example.fos.db.example;

import com.example.fos.core.Dessert;
import com.example.fos.core.MainCourse;
import com.example.fos.db.MealProvider;

import java.util.ArrayList;
import java.util.Collection;

public class ExampleMealProvider implements MealProvider {

    private static final Collection<MainCourse> mainCourses = new ArrayList<MainCourse>() {{
        add(new MainCourse("Pork roast with wine", 24.9, "Polish"));
        add(new MainCourse("Meatballs", 19.9, "Polish"));
        add(new MainCourse("Milanesas", 21.9, "Mexican"));
        add(new MainCourse("Chorizo", 15.9, "Mexican"));
        add(new MainCourse("Conchiglie", 19.9, "Italian"));
        add(new MainCourse("Garganelli", 17.9, "Italian"));
    }};

    private static final Collection<Dessert> desserts = new ArrayList<Dessert>() {{
        add(new Dessert("Cheesecake", 8.9, "Polish"));
        add(new Dessert("Alfajor", 7.9, "Mexican"));
        add(new Dessert("Ciarduna", 8.9, "Italian"));
    }};

    @Override
    public Collection<MainCourse> getMainCourses() {
        return mainCourses;
    }

    @Override
    public Collection<Dessert> getDessert() {
        return desserts;
    }
}
