package com.example.fos.db.example;

import com.example.fos.core.Product;
import com.example.fos.db.ProductProvider;

import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ExampleProductProvider implements ProductProvider {
    private ExampleDrinkProvider exampleDrinkProvider;
    private ExampleMealProvider exampleMealProvider;

    public ExampleProductProvider() {
        this.exampleDrinkProvider = new ExampleDrinkProvider();
        this.exampleMealProvider = new ExampleMealProvider();
    }

    @Override
    public Collection<Product> getProducts() {
        return Stream.of(exampleDrinkProvider.getAdditions(),
                exampleDrinkProvider.getDrinks(),
                exampleMealProvider.getDessert(),
                exampleMealProvider.getMainCourses())
                .flatMap(x -> x.stream())
                .collect(Collectors.toList());
    }
}
