package com.example.fos.io;

import java.io.PrintStream;
import java.util.Optional;

public abstract class Command {
    protected PrintStream stream;
    private Optional<Command> successor;

    protected Command(PrintStream stream) {
        this.stream = stream;
        this.successor = Optional.empty();
    }

    protected abstract void execute(String options);

    public void process(String command) {
        if(command == null)
            throw new IllegalArgumentException("command cannot be null");
        String commandName = extractCommandName(command);
        if (canProcess(commandName)) {
            String commandOption = command.replaceFirst(commandName, "").trim();
            execute(commandOption);
        }
        else
            successor
                    .orElseThrow(CommandNotFoundException::new)
                    .process(command);
    }

    private String extractCommandName(String command) {
        int firstSpaceIndex = command.indexOf(" ");
        int fixedIndex = firstSpaceIndex == -1 ? command.length() : firstSpaceIndex;

        return command.substring(0, fixedIndex);
    }

    public void setSuccessor(Command successor) {
        this.successor = Optional.of(successor);
    }

    private boolean canProcess(String name) {
        return getName().equals(name);
    }

    protected abstract String getName();
}
