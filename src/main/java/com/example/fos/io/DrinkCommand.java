package com.example.fos.io;

import com.example.fos.core.Drink;
import com.example.fos.core.Product;
import com.example.fos.db.DrinkProvider;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.example.fos.util.Collectors.singleItemCollector;

public class DrinkCommand extends RegexCommand {
    private DrinkProvider drinkProvider;

    public DrinkCommand(PrintStream stream, DrinkProvider drinkProvider) {
        super(stream);
        this.drinkProvider = drinkProvider;
    }

    @Override
    protected String getOptionsPatternFormat() {
        return String.format("drink=\"(%s)\"( addition=\"(%s)\")*", WORD_WITH_SPACES_PATTERN, WORD_WITH_SPACES_PATTERN);
    }

    @Override
    protected void printItems(Matcher matcher, String options) {
        stream.println("Order summary:");
        printDrink(matcher);
        printAdditions(options);
    }

    private void printDrink(Matcher matcher) {
        String drinkName = matcher.group(1);

        Drink drink = drinkProvider.getDrinks()
                .stream()
                .filter(refreshment -> drinkName.equals(refreshment.getName()))
                .collect(singleItemCollector());

        printFieldToStream("Drink:", drink.getName(), drink.getPrice());
    }

    private void printAdditions(String options) {
        Collection<String> additionNames = getAdditionNames(options, WORD_WITH_SPACES_PATTERN);

        drinkProvider.getAdditions()
                .stream()
                .filter(addition -> additionNames.contains(addition.getName()))
                .sorted(Comparator.comparing(Product::getName))
                .forEach(addition -> printFieldToStream("with", addition.getName(), addition.getPrice()));
    }

    private Collection<String> getAdditionNames(String options, String wordWithSpacesPattern) {
        Collection<String> additionNames = new ArrayList<>();
        Matcher matcher = Pattern
                .compile(String.format("addition=\"(%s)\"", wordWithSpacesPattern))
                .matcher(options);

        while (matcher.find())
            additionNames.add(matcher.group(1));

        return additionNames;
    }

    @Override
    protected String getName() {
        return "drink";
    }
}
