package com.example.fos.io;

import com.example.fos.core.MainCourse;
import com.example.fos.core.Product;
import com.example.fos.db.ProductProvider;

import java.io.PrintStream;
import java.util.Collection;
import java.util.Comparator;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.example.fos.util.Collectors.singleItemCollector;

public class GetCommand extends RegexCommand {
    private ProductProvider productProvider;

    public GetCommand(PrintStream stream, ProductProvider productProvider) {
        super(stream);
        this.productProvider = productProvider;
    }

    @Override
    protected String getOptionsPatternFormat() {
        return String.format("type=\"(%s)\"", WORD_WITH_SPACES_PATTERN);
    }

    @Override
    protected void printItems(Matcher matcher, String options) {
        String type = matcher.group(1);

        Collection<Product> products = productProvider.getProducts()
                .stream()
                .filter(product -> product.getType().toString().equals(type))
                .sorted(Comparator.comparing(Product::getName))
                .collect(Collectors.toList());

        if (products.isEmpty())
            stream.println(String.format("Product on type %s not found", type));

        products.forEach(product -> printFieldToStream(product.getName(), product.getPrice()));
    }

    @Override
    protected String getName() {
        return "get";
    }

    private void printFieldToStream(String name, double price) {
        stream.println(String.format(Locale.US, "%s\t%.2f", name, price));
    }
}
