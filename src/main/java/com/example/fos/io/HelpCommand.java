package com.example.fos.io;

import java.io.PrintStream;

public class HelpCommand extends Command {

    public HelpCommand(PrintStream stream) {
        super(stream);
    }

    @Override
    protected void execute(String options) {
        if (!options.isEmpty())
            throw new IllegalArgumentException(String.format("%s is not valid options for %s command", options, getName()));

        stream.println("Usage:");
        stream.println("lunch mainCourse=\"main course name\" dessert=\"dessert name\"");
        stream.println("drink drink=\"drink name\" [addition=\"addition name\"]*");
        stream.println("get type=\"type\"");
        stream.println("help");
    }

    @Override
    protected String getName() {
        return "help";
    }
}
