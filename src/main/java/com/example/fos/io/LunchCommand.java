package com.example.fos.io;

import com.example.fos.core.Dessert;
import com.example.fos.core.MainCourse;
import com.example.fos.db.MealProvider;

import java.io.PrintStream;
import java.util.regex.Matcher;

import static com.example.fos.util.Collectors.singleItemCollector;

public class LunchCommand extends RegexCommand {
    private final MealProvider mealProvider;

    public LunchCommand(PrintStream stream, MealProvider mealProvider) {
        super(stream);
        this.mealProvider = mealProvider;
    }

    @Override
    protected String getOptionsPatternFormat() {
        return String.format("mainCourse=\"(%s)\" dessert=\"(%s)\"", WORD_WITH_SPACES_PATTERN, WORD_WITH_SPACES_PATTERN);
    }

    @Override
    protected void printItems(Matcher matcher, String options) {
        stream.println("Order summary:");
        printMainCourse(matcher);
        printDessert(matcher);
    }

    private void printMainCourse(Matcher matcher) {
        String mainCourseName = matcher.group(1);
        MainCourse mainCourse = mealProvider.getMainCourses()
                .stream()
                .filter(meal -> mainCourseName.equals(meal.getName()))
                .collect(singleItemCollector());

        printFieldToStream("Main course:", mainCourse.getName(), mainCourse.getPrice());
    }

    private void printDessert(Matcher matcher) {
        String dessertName = matcher.group(3);
        Dessert dessert = mealProvider.getDessert()
                .stream()
                .filter(meal -> dessertName.equals(meal.getName()))
                .collect(singleItemCollector());

        printFieldToStream("Dessert:", dessert.getName(), dessert.getPrice());
    }


    @Override
    protected String getName() {
        return "lunch";
    }
}
