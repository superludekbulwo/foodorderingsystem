package com.example.fos.io;

import java.io.PrintStream;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class RegexCommand extends Command {
    protected RegexCommand(PrintStream stream) {
        super(stream);
    }

    protected static final String WORD_WITH_SPACES_PATTERN = "\\w+( \\w+)*";

    @Override
    protected void execute(String options) {
        Pattern pattern = Pattern.compile(getOptionsPatternFormat());
        Matcher matcher = pattern.matcher(options);

        if (!matcher.matches())
            throw new IllegalArgumentException(String.format("%s is not valid options for %s command", options, getName()));

        printItems(matcher, options);
    }

    protected void printFieldToStream(String field, String name, double price) {
        stream.println(String.format(Locale.US, "%s %s\t%.2f", field, name, price));
    }

    protected abstract String getOptionsPatternFormat();

    protected abstract void printItems(Matcher matcher, String options);
}
