package com.example.fos.util;

import java.util.stream.Collector;

public class Collectors {
    public static <T> Collector<T, ?, T> singleItemCollector() {
        return java.util.stream.Collectors.collectingAndThen(
                java.util.stream.Collectors.toList(),
                list -> {
                    if (list.size() != 1) {
                        throw new IllegalStateException();
                    }
                    return list.get(0);
                }
        );
    }
}
