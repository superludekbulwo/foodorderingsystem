package com.example.fos.core;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.*;

public class AdditionTest {

    public static final String SAMPLE_NAME = "SampleName";
    public static final double SAMPLE_PRICE = 2.0;

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void creatingAddition_WhenNameIsNull_ShouldThrowIllegalArgumentException() {
        Addition addition = new Addition(null, SAMPLE_PRICE);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void creatingAddition_WhenNameIsEmpty_ShouldThrowIllegalArgumentException() {
        Addition addition = new Addition("", SAMPLE_PRICE);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void creatingAddition_WhenPriceIsLessThan0_ShouldThrowIllegalArgumentException() {
        Addition addition = new Addition(SAMPLE_NAME, -SAMPLE_PRICE);
    }

    @Test
    public void gettingName_ShouldReturnName() {
        Addition addition = new Addition(SAMPLE_NAME, SAMPLE_PRICE);

        assertThat(addition.getName()).isEqualTo(SAMPLE_NAME);
    }

    @Test
    public void gettingPrice_ShouldReturnPrice() {
        Addition addition = new Addition(SAMPLE_NAME, SAMPLE_PRICE);

        assertThat(addition.getPrice()).isEqualTo(SAMPLE_PRICE);
    }

    @Test
    public void gettingType_ShouldReturnAddition() {
        Addition addition = new Addition(SAMPLE_NAME, SAMPLE_PRICE);

        assertThat(addition.getType()).isEqualTo(ProductType.ADDITION);
    }
}
