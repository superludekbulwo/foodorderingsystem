package com.example.fos.core;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class DessertTest {
    public static final String SAMPLE_NAME = "SampleName";
    public static final double PRICE = 2.0;
    public static final String SAMPLE_CUISINE = "SampleCuisine";

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void creatingDessert_WhenNameIsNull_ShouldThrowIllegalArgumentException() {
        Dessert dessert = new Dessert(null, PRICE, SAMPLE_CUISINE);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void creatingDessert_WhenNameIsEmpty_ShouldThrowIllegalArgumentException() {
        Dessert dessert = new Dessert("", PRICE, SAMPLE_CUISINE);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void creatingDessert_WhenPriceIsLessThan0_ShouldThrowIllegalArgumentException() {
        Dessert dessert = new Dessert(SAMPLE_NAME, -PRICE, SAMPLE_CUISINE);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void creatingDessert_WhenCuisineIsNull_ShouldThrowIllegalArgumentException() {
        Dessert dessert = new Dessert(SAMPLE_NAME, PRICE, null);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void creatingDessert_WhenCuisineIsEmpty_ShouldThrowIllegalArgumentException() {
        Dessert dessert = new Dessert(SAMPLE_NAME, PRICE, "");
    }

    @Test
    public void gettingName_ShouldReturnName() {
        Dessert dessert = new Dessert(SAMPLE_NAME, PRICE, SAMPLE_CUISINE);

        assertThat(dessert.getName()).isEqualTo(SAMPLE_NAME);
    }

    @Test
    public void gettingPrice_ShouldReturnPrice() {
        Dessert dessert = new Dessert(SAMPLE_NAME, PRICE, SAMPLE_CUISINE);

        assertThat(dessert.getPrice()).isEqualTo(PRICE);
    }

    @Test
    public void gettingCuisine_ShouldReturnPrice() {
        Dessert dessert = new Dessert(SAMPLE_NAME, PRICE, SAMPLE_CUISINE);

        assertThat(dessert.getCuisine()).isEqualTo(SAMPLE_CUISINE);
    }

    @Test
    public void gettingType_ShouldReturnDessert() {
        Dessert dessert = new Dessert(SAMPLE_NAME, PRICE, SAMPLE_CUISINE);

        assertThat(dessert.getType()).isEqualTo(ProductType.DESSERT);
    }
}
