package com.example.fos.core;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class DrinkTest {
    private static final String SAMPLE_NAME = "SampleName";
    private static final double PRICE = 2.0;

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void creatingDrink_WhenNameIsNull_ShouldThrowIllegalArgumentException() {
        new Drink(null, PRICE);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void creatingDrink_WhenNameIsEmpty_ShouldThrowIllegalArgumentException() {
        new Drink("", PRICE);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void creatingDrink_WhenPriceIsLessThan0_ShouldThrowIllegalArgumentException() {
        new Drink(SAMPLE_NAME, -PRICE);
    }

    @Test
    public void gettingName_ShouldReturnName() {
        Drink drink = new Drink(SAMPLE_NAME, PRICE);

        assertThat(drink.getName()).isEqualTo(SAMPLE_NAME);
    }

    @Test
    public void gettingPrice_ShouldReturnPrice() {
        Drink drink = new Drink(SAMPLE_NAME, PRICE);

        assertThat(drink.getPrice()).isEqualTo(PRICE);
    }

    @Test
    public void gettingType_ShouldReturnType() {
        Drink drink = new Drink(SAMPLE_NAME, PRICE);

        assertThat(drink.getType()).isEqualTo(ProductType.DRINK);
    }
}
