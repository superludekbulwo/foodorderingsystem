package com.example.fos.core;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class MainCourseTest {

    public static final String SAMPLE_NAME = "SampleName";
    public static final double PRICE = 2.0;
    public static final String SAMPLE_CUISINE = "SampleCuisine";

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void creatingMainCourse_WhenNameIsNull_ShouldThrowIllegalArgumentException() {
        MainCourse mainCourse = new MainCourse(null, PRICE, SAMPLE_CUISINE);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void creatingMainCourse_WhenNameIsEmpty_ShouldThrowIllegalArgumentException() {
        MainCourse mainCourse = new MainCourse("", PRICE, SAMPLE_CUISINE);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void creatingMainCourse_WhenPriceIsLessThan0_ShouldThrowIllegalArgumentException() {
        MainCourse mainCourse = new MainCourse(SAMPLE_NAME, -PRICE, SAMPLE_CUISINE);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void creatingMainCourse_WhenCuisineIsNull_ShouldThrowIllegalArgumentException() {
        MainCourse mainCourse = new MainCourse(SAMPLE_NAME, PRICE, null);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void creatingMainCourse_WhenCuisineIsEmpty_ShouldThrowIllegalArgumentException() {
        MainCourse mainCourse = new MainCourse(SAMPLE_NAME, PRICE, "");
    }

    @Test
    public void gettingName_ShouldReturnName() {
        MainCourse mainCourse = new MainCourse(SAMPLE_NAME, PRICE, SAMPLE_CUISINE);

        assertThat(mainCourse.getName()).isEqualTo(SAMPLE_NAME);
    }

    @Test
    public void gettingPrice_ShouldReturnPrice() {
        MainCourse mainCourse = new MainCourse(SAMPLE_NAME, PRICE, SAMPLE_CUISINE);

        assertThat(mainCourse.getPrice()).isEqualTo(PRICE);
    }

    @Test
    public void gettingCuisine_ShouldReturnPrice() {
        MainCourse mainCourse = new MainCourse(SAMPLE_NAME, PRICE, SAMPLE_CUISINE);

        assertThat(mainCourse.getCuisine()).isEqualTo(SAMPLE_CUISINE);
    }

    @Test
    public void gettingType_ShouldReturnMainCourse() {
        MainCourse mainCourse = new MainCourse(SAMPLE_NAME, PRICE, SAMPLE_CUISINE);

        assertThat(mainCourse.getType()).isEqualTo(ProductType.MAIN_COURSE);
    }
}
