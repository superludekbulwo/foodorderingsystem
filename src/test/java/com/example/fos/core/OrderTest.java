package com.example.fos.core;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

public class OrderTest {
    private Order.Builder builder;

    @BeforeMethod
    public void setUp() {
        builder = new Order.Builder();
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void addingProduct_WhenPassingNull_ShouldThrowIllegalArgumentException() {
        builder.addProduct(null);
    }

    @Test
    public void gettingProducts_WhenUsingEmptyBuilder_ShouldReturnEmptyCollection() {
        Order order = builder.build();

        assertThat(order.getProducts()).isEmpty();
    }

    @Test
    public void gettingProducts_WhenAddedProductToOrder_ShouldReturnProduct() {
        Product product = mock(Product.class);
        String productName = "SampleName";

        Order order = builder
                .addProduct(product)
                .build();

        assertThat(order.getProducts()).containsOnly(product);
    }
}