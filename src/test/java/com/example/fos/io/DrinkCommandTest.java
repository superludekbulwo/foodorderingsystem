package com.example.fos.io;

import com.example.fos.core.Addition;
import com.example.fos.core.Drink;
import com.example.fos.db.DrinkProvider;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class DrinkCommandTest {

    private DrinkCommand drinkCommand;
    private ByteArrayOutputStream outStream;
    private DrinkProvider drinkProvider;
    private Drink drink;
    private Addition sampleAddition1;
    private Addition sampleAddition2;

    @BeforeMethod
    public void setUp() {
        outStream = new ByteArrayOutputStream();
        PrintStream stream = new PrintStream(outStream);
        sampleAddition1 = mock(Addition.class);
        when(sampleAddition1.getName()).thenReturn("Sample Addition 1");
        when(sampleAddition1.getPrice()).thenReturn(0.5);

        sampleAddition2 = mock(Addition.class);
        when(sampleAddition2.getName()).thenReturn("Sample Addition 2");
        when(sampleAddition2.getPrice()).thenReturn(0.25);

        drink = mock(Drink.class);
        when(drink.getName()).thenReturn("Sample Drink");
        when(drink.getPrice()).thenReturn(1.9);

        drinkProvider = mock(DrinkProvider.class);
        when(drinkProvider.getDrinks()).thenReturn(Collections.singletonList(drink));
        when(drinkProvider.getAdditions()).thenReturn(new ArrayList<Addition>() {{
            add(sampleAddition1);
            add(sampleAddition2);
        }});

        drinkCommand = new DrinkCommand(stream, drinkProvider);
    }

    @Test
    public void processingDrink_ShouldPrintDrinkSummary() {
        drinkCommand.process("drink drink=\"Sample Drink\" addition=\"Sample Addition 1\"");
        String expectedString = "Order summary:\nDrink: Sample Drink\t1.90\nwith Sample Addition 1\t0.50\n";

        assertThat(outStream.toString()).isEqualTo(expectedString);
    }

    @Test
    public void processingDrink_WhenAdditionIsNotSet_ShouldPrintDrinkSummary() {
        drinkCommand.process("drink drink=\"Sample Drink\"");
        String expectedString = "Order summary:\nDrink: Sample Drink\t1.90\n";

        assertThat(outStream.toString()).isEqualTo(expectedString);
    }

    @Test
    public void processingDrink_WhenPassingMoreThanOneAddition_ShouldPrintDrinkSummary() {
        drinkCommand.process("drink drink=\"Sample Drink\" addition=\"Sample Addition 1\" addition=\"Sample Addition 2\"");
        String expectedString = "Order summary:\nDrink: Sample Drink\t1.90\nwith Sample Addition 1\t0.50\nwith Sample Addition 2\t0.25\n";

        assertThat(outStream.toString()).isEqualTo(expectedString);
    }

    @Test(expectedExceptions = CommandNotFoundException.class)
    public void processingDrink_WhenSuccessorIsNotSet_ShouldThrowCommandNotFoundException() {
        drinkCommand.process("other_command");
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void processingDrink_WhenPassingNull_ShouldThrowIllegalArgumentException() {
        drinkCommand.process(null);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void processingDrink_WhenDrinkIsNotSet_ShouldThrowIllegalArgumentException() {
        drinkCommand.process("drink addition=\"Sample Addition 1\"");
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void processingDrink_WhenPassingOptionInWrongOrder_ShouldThrowIllegalArgumentException() {
        drinkCommand.process("drink addition=\"Sample Addition 1\" drink=\"Sample Drink\"");
    }
}
