package com.example.fos.io;

import com.example.fos.core.Drink;
import com.example.fos.core.MainCourse;
import com.example.fos.core.Product;
import com.example.fos.core.ProductType;
import com.example.fos.db.ProductProvider;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GetCommandTest {
    private GetCommand get;
    private ByteArrayOutputStream outStream;
    private ProductProvider productProvider;
    private MainCourse mainCourse1;
    private MainCourse mainCourse2;
    private Drink drink1;

    @BeforeMethod
    public void setUp() {
        outStream = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(outStream);

        mainCourse1 = mock(MainCourse.class);
        when(mainCourse1.getName()).thenReturn("Main Course 1");
        when(mainCourse1.getType()).thenReturn(ProductType.MAIN_COURSE);
        when(mainCourse1.getPrice()).thenReturn(25.5);

        mainCourse2 = mock(MainCourse.class);
        when(mainCourse2.getName()).thenReturn("Main Course 2");
        when(mainCourse2.getType()).thenReturn(ProductType.MAIN_COURSE);
        when(mainCourse2.getPrice()).thenReturn(19.9);

        drink1 = mock(Drink.class);
        when(drink1.getName()).thenReturn("Drink 1");
        when(drink1.getType()).thenReturn(ProductType.DRINK);
        when(drink1.getPrice()).thenReturn(4.9);

        productProvider = mock(ProductProvider.class);
        when(productProvider.getProducts())
                .thenReturn(new ArrayList<Product>() {{
                    add(mainCourse1);
                    add(mainCourse2);
                    add(drink1);
                }});

        get = new GetCommand(printStream, productProvider);

    }

    @Test
    public void gettingProducts_ShouldReturnProductsOfSpecifiedType() {
        get.process("get type=\"MAIN_COURSE\"");

        assertThat(outStream.toString()).isEqualTo("Main Course 1\t25.50\nMain Course 2\t19.90\n");
    }

    @Test
    public void gettingProducts_WhenProductWithSpecifiedTypeDoesNotExist_ShouldReturnEmptyResult() {
        get.process("get type=\"DESSERT\"");

        assertThat(outStream.toString()).isEqualTo("Product on type DESSERT not found\n");
    }

    @Test
    public void gettingProducts_WhenTypeIsNotExist_ShouldReturnEmptyResult() {
        get.process("get type=\"Other Type\"");

        assertThat(outStream.toString()).isEqualTo("Product on type Other Type not found\n");
    }

    @Test(expectedExceptions = CommandNotFoundException.class)
    public void gettingProducts_WhenSuccessorIsNotSet_ShouldThrowCommandNotFoundException() {
        get.process("other_command");
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void gettingProducts_WhenPassingNull_ShouldThrowIllegalArgumentException() {
        get.process(null);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void gettingProducts_WhenTypeIsNotSet_ShouldThrowIllegalArgumentException() {
        get.process("get");
    }
}
