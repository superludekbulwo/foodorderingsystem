package com.example.fos.io;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.assertj.core.api.Assertions.assertThat;

public class HelpCommandTest {
    private HelpCommand help;
    private ByteArrayOutputStream outStream;

    @BeforeMethod
    public void setUp() {
        outStream = new ByteArrayOutputStream();
        PrintStream stream = new PrintStream(outStream);
        help = new HelpCommand(stream);
    }

    @Test
    public void processingHelp_ShouldPrintHelpOnPrintStream() {
        help.process("help");

        assertThat(outStream.toString()).isNotEmpty();
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void processingHelp_WhenUsingOption_ShouldThrowIllegalArgumentException() {
        help.process("help option");
    }

    @Test(expectedExceptions = CommandNotFoundException.class)
    public void processingHelp_WhenSuccessorIsNotSet_ShouldThrowCommandNotFoundException() {
        help.process("other_command");
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void processingHelp_WhenPassingNull_ShouldThrowIllegalArgumentException() {
        help.process(null);
    }
}
