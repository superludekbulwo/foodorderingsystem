package com.example.fos.io;

import com.example.fos.core.Dessert;
import com.example.fos.core.MainCourse;
import com.example.fos.core.ProductType;
import com.example.fos.db.MealProvider;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class LunchCommandTest {
    private LunchCommand lunch;
    private ByteArrayOutputStream outStream;
    private MainCourse mainCourse;
    private Dessert dessert;

    @BeforeMethod
    public void setUp() {
        outStream = new ByteArrayOutputStream();
        PrintStream stream = new PrintStream(outStream);
        mainCourse = mock(MainCourse.class);
        when(mainCourse.getType()).thenReturn(ProductType.MAIN_COURSE);
        when(mainCourse.getName()).thenReturn("Sample Main Course");
        when(mainCourse.getCuisine()).thenReturn("SampleCuisine");
        when(mainCourse.getPrice()).thenReturn(2.5);

        dessert = mock(Dessert.class);
        when(dessert.getType()).thenReturn(ProductType.DESSERT);
        when(dessert.getName()).thenReturn("Sample Dessert");
        when(dessert.getCuisine()).thenReturn("SampleCuisine");
        when(dessert.getPrice()).thenReturn(1.9);

        MealProvider mealProvider = mock(MealProvider.class);

        when(mealProvider.getMainCourses()).thenReturn(Collections.singletonList(mainCourse));
        when(mealProvider.getDessert()).thenReturn(Collections.singletonList(dessert));

        lunch = new LunchCommand(stream, mealProvider);
    }

    @Test
    public void processingLunch_ShouldPrintLunchSummary() {
        lunch.process("lunch mainCourse=\"Sample Main Course\" dessert=\"Sample Dessert\"");
        String expectedString = "Order summary:\nMain course: Sample Main Course\t2.50\nDessert: Sample Dessert\t1.90\n";

        assertThat(outStream.toString()).isEqualTo(expectedString);
    }

    @Test(expectedExceptions = CommandNotFoundException.class)
    public void processingLunch_WhenSuccessorIsNotSet_ShouldThrowCommandNotFoundException() {
        lunch.process("other_command");
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void processingLunch_WhenPassingNull_ShouldThrowIllegalArgumentException() {
        lunch.process(null);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void processingLunch_WhenMainCourseIsNotSet_ShouldThrowIllegalArgumentException() {
        lunch.process("lunch dessert=\"Sample Dessert\"");
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void processingLunch_WhenDessertIsNotSet_ShouldThrowIllegalArgumentException() {
        lunch.process("lunch mainCourse=\"Sample Main Course\"");
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void processingLunch_WhenPassingOptionInWrongOrder_ShouldThrowIllegalArgumentException() {
        lunch.process("lunch dessert=\"Sample Dessert\"  mainCourse=\"Sample Main Course\"");
    }
}
