package com.example.fos.util;

import com.example.fos.util.Collectors;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;

public class CollectorsTest {
    @Test
    public void collectingItems_WhenCollectionContainsSingleItem_ShouldReturnItem() {
        Collection<Integer> collection = Collections.singletonList(5);

        assertThat(collection.stream().collect(Collectors.singleItemCollector())).isEqualTo(5);
    }

    @Test(expectedExceptions = IllegalStateException.class)
    public void collectingItems_WhenCollectionContainsMoreThanOneItem_ShouldThrowIllegalStateException() {
        new ArrayList<Integer>(){{add(5); add(8);}}.stream().collect(Collectors.singleItemCollector());
    }

    @Test(expectedExceptions = IllegalStateException.class)
    public void collectingItems_WhenCollectionIsEmpty_ShouldThrowIllegalStateException() {
        new ArrayList<Integer>().stream().collect(Collectors.singleItemCollector());
    }
}
